﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    class Game
    {
        public enum Value
        {
            X,
            O,
            None,
            Equal
        };

        public Game()
        {
            _mtx = new Value[3, 3];
            NewGame();
        }

        public void NewGame()
        {
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                    _mtx[i, j] = Value.None;

            _currentValue = Value.X;
        }

        public bool Insert(int row, int column)
        {
            if (_mtx[row, column] == Value.None)
            {
                _mtx[row, column] = _currentValue;
                _currentValue = (_currentValue == Value.X) ? Value.O : Value.X;
                return true;
            }
            return false;
        }

        public Value GetCurrentValue()
        {
            return _currentValue;
        }

        public Value GetValue(int row, int column)
        {
            return _mtx[row, column];
        }

        public Value Result()
        {
            if ((_mtx[0, 0] == _mtx[0, 1] && _mtx[0, 0] == _mtx[0, 2]) ||
                (_mtx[0, 0] == _mtx[1, 1] && _mtx[0, 0] == _mtx[2, 2]) ||
                (_mtx[0, 0] == _mtx[1, 0] && _mtx[0, 0] == _mtx[2, 0]) )
                return _mtx[0, 0];

            if ((_mtx[1, 1] == _mtx[1, 0] && _mtx[1, 1] == _mtx[1, 2]) ||
                (_mtx[1, 1] == _mtx[0, 1] && _mtx[1, 1] == _mtx[2, 1]) ||
                (_mtx[1, 1] == _mtx[0, 2] && _mtx[1, 1] == _mtx[2, 0]))
                return _mtx[1, 1];

            if ((_mtx[2, 2] == _mtx[2, 0] && _mtx[2, 2] == _mtx[2, 1]) ||
                (_mtx[2, 2] == _mtx[0, 2] && _mtx[2, 2] == _mtx[1, 2]))
                return _mtx[2, 2];

            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                    if (_mtx[i, j] == Value.None)
                        return Value.None;

            return Value.Equal;
        }

        private Value[,] _mtx;
        private Value _currentValue;
    }
}
