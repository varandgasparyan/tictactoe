﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace TicTacToe
{
    public partial class startPage : PhoneApplicationPage
    {
        public startPage()
        {
            InitializeComponent();

            m_game = new Game();
            _xCount = 0;
            _oCount = 0;
            _flag = true;
        }

        private void CheckResult()
        {
            Game.Value winner = m_game.Result();
            if (winner == Game.Value.Equal)
            {
                finish.Text = "No Winner.";
                finishGrid.Opacity = 0.8;
                finishGrid.Visibility = Visibility.Visible;
                _flag = false;
            }
            else if (!(winner == Game.Value.None))
            {
                finish.Text = (winner == Game.Value.X ? "X won" : "O won");
                if (winner == Game.Value.X)
                {
                    ++_xCount;
                    x_count.Text = "X : " + _xCount.ToString();
                }
                else
	            {
                    ++_oCount;
                    O_count.Text = "O : " + _oCount.ToString();
                }
                finishGrid.Visibility = Visibility.Visible;
                _flag = false;
            }
        }

        private Game m_game;
        private int _xCount;
        private int _oCount;
        private bool _flag;
        
        private void label_0_0_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_flag)
            {
                Game.Value crtValue = m_game.GetCurrentValue();
            if (m_game.Insert(0, 0))
            {
                if (crtValue == Game.Value.X)
                    label_0_0.Text = "X";
                else
                    label_0_0.Text = "O";
            }
            CheckResult();
            }
        }

        private void label_0_1_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_flag)
            {
              Game.Value crtValue = m_game.GetCurrentValue();
            if (m_game.Insert(0, 1))
            {
                if (crtValue == Game.Value.X)
                    label_0_1.Text = "X";
                else
                    label_0_1.Text = "O";
            }
            CheckResult();  
            }     
        }

        private void label_0_2_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_flag)
            {
                Game.Value crtValue = m_game.GetCurrentValue();
            if (m_game.Insert(0, 2))
            {
                if (crtValue == Game.Value.X)
                    label_0_2.Text = "X";
                else
                    label_0_2.Text = "O";
            }
            CheckResult();
            }
            
        }

        private void label_1_0_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_flag)
            {
                Game.Value crtValue = m_game.GetCurrentValue();
            if (m_game.Insert(1, 0))
            {
                if (crtValue == Game.Value.X)
                    label_1_0.Text = "X";
                else
                    label_1_0.Text = "O";
            }
            CheckResult();
            } 
        }

        private void label_1_1_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_flag)
            {
               Game.Value crtValue = m_game.GetCurrentValue();
            if (m_game.Insert(1, 1))
            {
                if (crtValue == Game.Value.X)
                    label_1_1.Text = "X";
                else       
                    label_1_1.Text = "O";
            }
            CheckResult(); 
            }
        }

        private void label_1_2_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_flag)
            {
                Game.Value crtValue = m_game.GetCurrentValue();
            if (m_game.Insert(1, 2))
            {
                if (crtValue == Game.Value.X)
                    label_1_2.Text = "X";
                else
                    label_1_2.Text = "O";
            }
            CheckResult();
            }     
        }

        private void label_2_0_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_flag)
            {
             Game.Value crtValue = m_game.GetCurrentValue();
            if (m_game.Insert(2, 0))
            {
                if (crtValue == Game.Value.X)
                    label_2_0.Text = "X";
                else
                    label_2_0.Text = "O";
            }
            CheckResult();   
            }
        }

        private void label_2_1_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_flag)
            {
                Game.Value crtValue = m_game.GetCurrentValue();
            if (m_game.Insert(2, 1))
            {
                if (crtValue == Game.Value.X)
                    label_2_1.Text = "X";
                else
                    label_2_1.Text = "O";
            }
            CheckResult();
            }    
        }

        private void label_2_2_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_flag)
            {
               Game.Value crtValue = m_game.GetCurrentValue();
            if (m_game.Insert(2, 2))
            {
                if (crtValue == Game.Value.X)
                    label_2_2.Text = "X";
                else
                    label_2_2.Text = "O";
            }
            CheckResult(); 
            }  
        }

        private void Button_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            m_game.NewGame();
            label_0_0.Text = "";
            label_0_1.Text = "";
            label_0_2.Text = "";
            label_1_0.Text = "";
            label_1_1.Text = "";
            label_1_2.Text = "";
            label_2_0.Text = "";
            label_2_1.Text = "";
            label_2_2.Text = "";
            finishGrid.Visibility = Visibility.Collapsed;
            _flag = true;
        }

        private void MenuButtonTapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml" , UriKind.Relative));
        }
    }
}